//Establecemos las variables para el procesamiento de información
var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    sanitizer = require('sanitizer'),
    io = require('socket.io').listen(server, {
        'log level' : 2
    });

server.listen(3000);

//Definimos los archivos estaticos (GUI) para los usuarios
app.use(express.static(__dirname + '/../client'));
app.get('/../client', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});

//Variables Logica del negocio
//usuarios: Encargada de almacenar los usuarios validos que ingresen al sistema.
//jugadores: Encargada de almacenar a los dos contendientes de la partida.
var usuarios = [],
	jugadores = new Array(),
	jugador,
	turno = false;


/*  =======================================================================================
	CONNECTION: Cuando un usuario se conecta al servidor*/
io.sockets.on('connection', function (socket){
	/*  -----------------------------------------------------------------------------------
        CONEXION: Emitimos la info del tablero de jugadores al usuario que ingreso*/
    var size = jugadores.length;
    if(size%2 != 0){
        socket.emit('conexion', jugadores[size-1]);
    }

	/*  -----------------------------------------------------------------------------------
		COMPROBAR USUARIO: Recibimos y validamos el usuario que intenta loguearse*/
    socket.on('comprobarUsuario',function(data, callback){

    	//Limpiamos la información que llega
        data = sanitizer.escape(data);

        //Comprobamos que el nombre no esta en uso, o contiene caracteres raros.
        if(usuarios.indexOf(data) >= 0){
            callback({ok : false, msg : 'Nombre de usuario en uso'});
        }else{
            //Enviamos el nick comprobado al usuario.
            callback({ok : true, nick : data});
            socket.nick = data;
            usuarios.push(data);
            console.log('Usuario conectado: ' + socket.nick);
        }

    });

    /*  -----------------------------------------------------------------------------------
		NUEVO JUGADOR: Recibimos la petición de un Usuario que quiere unirse a la partida*/
    socket.on('nuevoJugador', function(data, callback){
        if(!socket.jugador ){
        	jugador = new Object();
        	jugador.nick = socket.nick;
            jugador.puntaje = 0;
            jugador.numero = jugadores.length+1;
            jugador.estado = 'Esperando';

            jugadores.push(jugador);
            socket.jugador = jugador.numero;
            var size = jugadores.length;
            var jugadores_actu = [];

            if ( jugadores.length%2 == 0 )
                jugadores_actu = [jugadores[size-2], jugadores[size-1]];
            else
                jugadores_actu = [jugadores[size-1]];

            callback({
                ok : true,
                gamers : jugadores_actu
            });

            io.sockets.emit('nuevoJugador', jugadores_actu);
        }

    });
});