jQuery(document).ready(function(){
	//Realizamos la conexión con el Servidor
	var socket = io.connect();

	//usuarios: Almacenamos todos los nombres de usuarios
	//turno: Determina si es el turno del jugador
	//start: Determina si el juego ya inicio o no
	//barco: Determina el barco seleccionado en la armada
	//reservas: Cantidad 
	var nick = "", usuarios = [], gamers = [],
		turno = false,
		start = false,
		barco = 0,
		number_boats = [6, 4, 2],
		campo = ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
		batalla = ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
		boat_type = "",
		boat_orient = "positive";

	/*  ==================================================================================
		CONEXION: Usuario conectado recibe la información del tablero de jugadores*/
	socket.on('conexion', function(data){
		if (data) {
			gamers.push(data);
			updateGamerTamplate(1, gamers[0].nick, gamers[0].puntaje, "gamer", gamers[0].estado);
		}
	});

	/*  ==================================================================================
		NUEVO JUGADOR:  ACTUALIZAMOS EL TABLERO DE JUGADORES*/
	socket.on('nuevoJugador', function(data) {
		if ( data.length == 2 ) {
			if (gamers.length == 0) {
				gamers = data;
				updateGamerTamplate(2, gamers[1].nick, gamers[1].puntaje, "gamer", gamers[1].estado);
			}
			else if (gamers.length == 1) {
				gamers.length = 0;
				updateGamerTamplate(1, '??????????', '??', "question", "Esperando");
			}
		}
		else {
			if (!socket.jugador) {
				gamers = data;
				updateGamerTamplate(1, gamers[0].nick, gamers[0].puntaje, "gamer", gamers[0].estado);
			}
		}		
	});

	/*  ==================================================================================
		LOGIN: Usuario solicita loguearse en el sistema*/
	$('#btn_login').on('click', function(){
		//obetenmos el contenido de la caja de texto
		var nombre = document.getElementById('nickname').value;

		//comprobamos la valides del contenido
		if( nombre !== '' ){
			//emitimos al servidor la información del servidor y esperamos la respuesta
			socket.emit('comprobarUsuario',nombre, function(data){

				//Si no es valido el nombre se muestra un mensaje de error
				if(!data.ok){
					$('#login p.error').html(data.msg);
				}else{
					nick = data.nick;

					$('#login h3').text(nick);
					$('#login h3').addClass("usuario-login");
					$('#login p.error').slideUp('slow');
					$('#login input[type=text]').slideUp('slow');
					$('#login button.btn').slideUp('slow');
					$('#login button.btn').attr('disabled');
				}

			});
		}

	});

	/*  ==================================================================================
		UNIRSE: Usuario solicita unirse a la partida*/
	$('#btn_unirse').on('click', function(){
		if( !socket.jugador && nick != ""){
			socket.emit('nuevoJugador', nick, function(data){
				if( data.ok ){
					socket.jugador = data.gamers[data.gamers.length-1].numero;
					if(gamers.length == 1){
						gamers.push(data.gamers[1]);
						updateGamerTamplate(2, gamers[1].nick, gamers[1].puntaje, "gamer", gamers[1].estado);
					}
					else
						updateGamerTamplate(1, data.gamers[0].nick, data.gamers[0].puntaje, "gamer", data.gamers[0].estado);

					$('#btn_unirse').addClass('add-part');
					$('#btn_unirse').html("Ok");
				}
			});
		}
	});

	$('#btn')

	/*  ==================================================================================
		EACH: Identificamos cada celda del tablero de opciones */
	$('.selc').each(function(e){
		$(this).attr({ 'data-celda' : parseInt(e, 10)+1 });
	});

	/*  ==================================================================================
		EACH: Identificamos cada celda del tablero de opciones */
	$('#tablero-campo .columna').each(function(e){
		$(this).attr({ 'data-celda' : parseInt(e, 10)+1 });
	});

	/*  ==================================================================================
		CLICK: Evento al seleccionar con un click uno de los barcos de armada */
	$('.selc').on('click', function(){
		if (barco != 0){
			$(`#opciones-contenedor #barco${barco}`).css('background-color','');
			$(`#opciones-contenedor #barco${barco}`).css('color','');
		}
		
		barco = parseInt($(this).attr('data-celda'));
		if (barco == 1 || barco == 4) boat_type = 1;
		else if (barco == 2 || barco == 5) boat_type = 2;
		else boat_type = 3;

		//cambiamos la imagen de la selección actual
		$(`#opciones-contenedor #barco${barco}`).css('background-color','#CA1640');
		$(`#opciones-contenedor #barco${barco}`).css('color','white');
	});

	/*  ==================================================================================
		CLICK: Evento al seleccionar un punto en el propio territorio del jugador */
	$('#tablero-campo .columna').on('click', function(){
		var celda = parseInt($(this).attr('data-celda'));
		var cardin = "H";
		var nsum = 1;

		if (!$(`.item #rd_hor`).prop('checked')) {
			cardin = "V";
			nsum = 8;
		}

		//Comprobamos si podemos ubicar barcos en nuestra zona
		if (validLocation(celda, cardin)) locateBoats(boat_type, celda, boat_orient, nsum);
	});
	/*  ==================================================================================
		FUNCTION - updateGamerTamplate: Permite actualizar la plantilla para jugadores */
	function updateGamerTamplate (numero, nick, puntaje, img, estado) {
		$('#jugador' + numero + '-nombre').html(nick);
		$('#jugador' + numero + '-puntaje').html(puntaje);
		$('#jugador' + numero + '-status').html(estado);
		$('#jugador' + numero + ' .avatar').css('background-image',`url("../img/${img}.png")`);	
	}
	/*  ==================================================================================
		FUNCTION - validLocation: Permite determinar si en una celda seleccionada se puede
				   ubicar un barco */
	function validLocation (cl, cd) {
		if (socket.jugador && checkBoats() && !start) {
			var nsum = 1;
			if (cd != "H") nsum = 8;
			
			switch (boat_type) {
				case 1:
					boat_orient = "positive";
					return tourFields(boat_type, cl, "positive", nsum);
					break;
				case 2:
					if (nsum == 8) {
						if (cl > 56 && cl < 65){
							boat_orient = "negative";
							return tourFields(boat_type, cl, "negative", nsum);
						} 
					}
					else {
						if (cl%8 == 0){
							boat_orient = "negative";
							return tourFields(boat_type, cl, "negative", nsum);
						}
					}

					boat_orient = "positive";
					return tourFields(boat_type, cl, "positive", nsum);
					break;
				case 3:
					if (nsum == 8) {
						if ((cl > 56 && cl <65) || ((cl+nsum) > 56 && (cl+nsum) < 65)){
							boat_orient = "negative";
							return tourFields(boat_type, cl, "negative", nsum);
						}
					}
					else {
						if (cl%8 == 0 || (cl+1)%8 == 0){
							boat_orient = "negative";
							return tourFields(boat_type, cl, "negative", nsum);
						} 
					}

					boat_orient = "positive";
					return tourFields(boat_type, cl, "positive", nsum);
					break;
			}
		}
		return false;
	}
	/*  ==================================================================================
		FUNCTION - tourFields: Recorre los campos del tablero, validando su estado*/
	function tourFields (n, cld, dir, sum) {
		for (var i = 0; i<n; i++) {
			if (dir == "positive") {
				if(campo[cld+(sum*i)] != "") return false;
			}
			else {
				if(campo[cld-(sum*i)] != "") return false;
			}
		}
		return true;
	}
	/*  ==================================================================================
		FUNCTION - locateBoats: Recorre los campos del tablero e ingresa las respectivas
				   imagenes*/
	function locateBoats (n, cld, dir, sum) {
		var res = boat_type-1;
		var second_pos = 0;
		number_boats[boat_type-1] -= 1;

		if(barco >= 1 && barco <= 3) second_pos = barco+3;
		else second_pos = barco-3;

		$(`#opciones-contenedor #barco${barco} .data_barco p:last`)
		.text(`CANTIDAD: ${number_boats[boat_type-1]}x`);
		$(`#opciones-contenedor #barco${second_pos} .data_barco p:last`)
		.text(`CANTIDAD: ${number_boats[boat_type-1]}x`);

		for (var i = 0; i<n; i++) {
			if (dir == "positive") {
				if (sum == 8) loadBoat(cld+(sum*i), barco, i, true);
				else loadBoat(cld+(sum*i), barco, i, false);
			}
			else {
				if (sum == 8) loadBoat(cld-(sum*i), barco, res, true);
				else loadBoat(cld-(sum*i), barco, res, false);
				res--;
			}
		}
	}
	/*  ==================================================================================
		FUNCTION - loadBoat: Carga un barco en una determinada posición*/
	function loadBoat (pos, boat, num, rot) {
		if (rot) {
			$(`#tablero-campo .columna[data-celda="${pos}"] img`)
			.addClass("rotate_img");
		}

		campo[pos] = "B";
		$(`#tablero-campo .columna[data-celda="${pos}"] img`)
			.attr('src',`../img/barco${boat}-${num}.png`);
	}
	/*  ==================================================================================
		FUNCTION - checkBoats: Coprueba si se pueden añadir mas barcos de ese tipo*/
	function checkBoats () {
		if (barco != 0) {
			var band = true;
			for (var i = 0; i < 3; i++) 
				if (number_boats[i] > 0) band = false;
			if (band) {
				$(`#jugador${socket.jugador}-status`).html("LISTO");
			}
			if (number_boats[boat_type-1] > 0) return true;
			return false;
		}

		return false;
	}
});